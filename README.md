# DataForSeo

[![Hex version badge](https://img.shields.io/hexpm/v/data_for_seo.svg)](https://hex.pm/packages/data_for_seo)

DataForSeo client library for Elixir.

It only supports very limited set of functions yet. Refer to [data_for_seo.ex](https://gitlab.com/egze/data_for_seo/blob/master/lib/data_for_seo.ex) for available functions and examples.

## Docs

Docs can be found at [https://hexdocs.pm/data_for_seo](https://hexdocs.pm/data_for_seo)

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `data_for_seo` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:data_for_seo, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/data_for_seo](https://hexdocs.pm/data_for_seo).

