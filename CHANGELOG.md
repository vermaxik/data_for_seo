# Changelog

## unreleased

* Refactor datetime conversion in Parser. https://gitlab.com/egze/data_for_seo/-/merge_requests/19


## 0.3.0 (2020-03-30)

Breaking change.
* Renamed functions to use same names as API endpoints.
* Switched to response structs that mimic API 1:1.


## 0.2.0 (2020-03-11)

Breaking change. Switched to V3 API.
